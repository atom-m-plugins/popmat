<?php

class popmat {

    // Marker for plugin
    private $DB;

    public function __construct($params) {
        $this->DB = getDB();

    }


    public function common($params) {

        $config = json_decode(file_get_contents(dirname(__FILE__).'/config.json'), true);

        $Cache = new Cache;
        $Cache->lifeTime = 3600;
        if ($Cache->check('pl_popmat')) {
            $mats = $Cache->read('pl_popmat');
            $mats = unserialize($mats);
        } else {
            $mats = $this->DB->select($config['module'], DB_ALL, array(
                'order' => '`'.$config['sort'].'` DESC',
                'limit' => $config['limit']));
            $Cache->write(serialize($mats), 'pl_popmat', array());
            $author = $this->DB->select('users', DB_ALL);
        }


        if (!empty($mats)) {

            foreach ($mats as $key => $mat) {

                // Attaches img
                $mats[$key]['img_url'] = function() use($mat, $config) {
                    $images = $this->DB->select($config['module'].'_attaches', DB_ALL, array('cond' => array('entity_id' =>  $mat['id'])));
                    if (count($images) > 0) {
                        return $this->smallImage($images[0]['filename'], $config['module']);
                    } else {
                        return;
                    }
                };

                // Author of material
                $mats[$key]['author_name'] = function() use($mat) {
                    $author = $this->DB->select('users', DB_ALL, array('cond' => array('id' =>  $mat['author_id'])));
                    return $author[0]['name'];
                };

                $mats[$key]['author_url'] = get_url('/users/info/' . $mat['author_id']);
                $mats[$key]['url'] = get_url(matUrl($mat['id'], $mat['title'], $config['module']));
                $mats[$key]['title'] = trim(mb_substr($mats[$key]['title'], 0, $config['short_title']));
                $announce = $mats[$key]['main'];
                $announce = Register::getClass('PrintText')->getAnnounce($announce, $mats[$key]['url'], 0, $config['short_main']);
                $mats[$key]['main'] = $announce;

                $mats[$key]['date'] = atmDate($mats[$key]['date']);
                $mats[$key]['key_sort'] = $mats[$key][$config['sort']];
                if ($config['sort'] == 'date') {
                    $mats[$key]['key_sort'] = atmDate($mats[$key]['key_sort']);
                }
            }
            $params['plugin_popmat'] = $mats;
        }

        return $params;
    }

    public function getImagesPath($file = null, $module = null, $size_x = null, $size_y = null)
    {
        if (!isset($module)) $module = $this->module;
        $path = '/data/images/' . $module . '/' . (($size_x !== null and $size_y !== null) ? $size_x . 'x' . $size_y . '/' : '') . (!empty($file) ? $file : '');
        return $path;
    }

    public function smallImage($filename, $module)
    {
        // Ищем настройки миниатюр в конфигурации плагина
        if (isset($this->config['use_local_preview']) and $this->config['use_local_preview']) {
            $preview = $this->config['use_preview'];
            $size_x = $this->config['img_size_x'];
            $size_y = $this->config['img_size_y'];
        // Иначе в конфигурации модуля
        } elseif (Config::read('use_local_preview', $module)) {
            $preview = Config::read('use_preview', $module);
            $size_x = Config::read('img_size_x', $module);
            $size_y = Config::read('img_size_y', $module);
        // Если опять пусто, то основкой конфигурации сайта
        } else {
            $preview = Config::read('use_preview');
            $size_x = Config::read('img_size_x');
            $size_y = Config::read('img_size_y');
        }
        $image_link = get_url($this->getImagesPath($filename, $module));
        $preview_link = $image_link;

        if ($preview) {
            if (file_exists(R.'/'.$this->getImagesPath($filename, $module, $size_x, $size_y))) {
                $preview_link = get_url($this->getImagesPath($filename, $module, $size_x, $size_y));
            } else {
                // Узнаем, а нужно ли превью для изображения
                list($width, $height, $type, $attr) = @getimagesize(R . $image_link);
                if ((empty($width) and empty($height)) or ($width > $size_x or $height > $size_y)) {
                    $preview_link = get_url($this->getImagesPath($filename, $module, $size_x, $size_y));
                }
            }
        }

        return $preview_link;
    }
}
