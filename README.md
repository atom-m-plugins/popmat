## Плагин "Топ материалов"

Версия: 0.5
Лицензия GNU GPL

***

Выводит топ материалов (по просмотрам)

## Установка:

1. Распаковать архив и переместить папку popmat в /plugins.
2. Для файлов /plugins/popmat/config.json и /plugins/popmat/templates/popmat.html выставить права 777.
3. Добавить метку в нужном месте шаблона:

```
    {% if atm.plugin_popmat %}
        {% include '@popmat/list.html.twig' %}
    {% endif %}
```